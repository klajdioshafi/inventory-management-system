package service;

import dto.ProductRequest;
import entity.Product;

import java.util.List;

public interface ProductService {

    void createProduct(ProductRequest productRequest);
    List<Product> notifyForLowStock();
}
