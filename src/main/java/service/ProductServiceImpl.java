package service;

import converter.CategoryConverter;
import converter.ProductConverter;
import dao.CategoryDAO;
import dao.CategoryDAOImpl;
import dao.ProductDAO;
import dao.ProductDAOImpl;
import dto.CategoryRequest;
import dto.ProductRequest;
import entity.Category;
import entity.Product;
import org.hibernate.SessionFactory;
import util.HibernateUtil;
import validator.ProductValidator;

import java.util.List;
import java.util.stream.Collectors;

public class ProductServiceImpl implements ProductService, CategoryService{

    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    private final ProductDAO productDAO = new ProductDAOImpl(sessionFactory.createEntityManager());
    private final CategoryDAO categoryDAO = new CategoryDAOImpl(sessionFactory.createEntityManager());

    private final Integer limitStock = 5;

    @Override
    public List<Product> notifyForLowStock() {
       return productDAO.findAll().stream().filter(product -> product.getQuantity() < limitStock).collect(Collectors.toList());
    }

    @Override
    public void createProduct(ProductRequest productRequest) {
        ProductValidator.validateProduct(productRequest);
        Product product = ProductConverter.convertRequestToEntity(productRequest);
        Category category = categoryDAO.findById(product.getCategory().getId());
        product.setCategory(category);
        productDAO.save(product);
        System.out.println(productRequest);
    }

    @Override
    public void createCategory(CategoryRequest categoryRequest) {
        Category category = CategoryConverter.convertRequestToEntity(categoryRequest);
        categoryDAO.save(category);
        System.out.println(categoryRequest);
    }
}
