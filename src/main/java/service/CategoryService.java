package service;

import dto.CategoryRequest;

public interface CategoryService {

    void createCategory(CategoryRequest categoryRequest);
}
