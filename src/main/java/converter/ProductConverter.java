package converter;

import dto.ProductRequest;
import entity.Category;
import entity.Product;

public class ProductConverter {

    public static Product convertRequestToEntity(ProductRequest productRequest) {

        Product product = new Product();
        product.setTitle(productRequest.getTitle());
        product.setDescription(productRequest.getDescription());
        product.setCategory(new Category(productRequest.getCategoryId()));
        product.setPrice(productRequest.getPrice());
        product.setQuantity(productRequest.getQuantity());


        return product;
    }
}
