package converter;

import dto.CategoryRequest;
import entity.Category;

import java.util.List;

public class CategoryConverter {

    public static Category convertRequestToEntity(CategoryRequest categoryRequest) {

        Category category = new Category();
        category.setTitle(categoryRequest.getTitle());
        category.setProducts(List.of());
        return category;
    }
}
