package dao;

import entity.Category;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;

import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {

    private final EntityManager entityManager;

    public CategoryDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Category category) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        try {
            entityManager.persist(category);
            transaction.commit();
        }catch (Exception e) {
            transaction.rollback();
            System.out.println(e.getMessage());
            System.out.println("Category may exist in the database.");
        }
    }

    public List<Category> findAll() {
        return entityManager.createQuery("SELECT c FROM category c", Category.class).getResultList();
    }

    public Category findById(Long id) {
        Category category = entityManager.createQuery("SELECT c FROM category c WHERE c.id = " + id, Category.class).getResultList().getLast();
        System.out.println(category);
        return category;
    }

    public void updateCategory(Category category){
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        try {
            entityManager.merge(category);
            transaction.commit();
        }catch (Exception e) {
            transaction.rollback();
            System.out.println(e.getMessage());
            System.out.println("Category may exist in the database.");
        }
    }
}
