package dao;

import entity.Product;

import java.util.List;

public interface ProductDAO {

    void save(Product product);

    List<Product> findAll();
    Product findById(Long id);
}
