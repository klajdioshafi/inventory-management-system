package dao;

import entity.Category;

import java.util.List;

public interface CategoryDAO {

    void save(Category category);

    List<Category> findAll();
    Category findById(Long id);

    void updateCategory(Category category);
}
