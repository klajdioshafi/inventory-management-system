package dao;

import entity.Product;
import jakarta.persistence.EntityManager;

import java.util.List;

public class ProductDAOImpl implements ProductDAO {

    private final EntityManager entityManager;

    public ProductDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Product> findAll() {
        return entityManager.createQuery("SELECT p FROM Product p", Product.class).getResultList();
    }

    public Product findById(Long id) {
        return entityManager.createQuery("SELECT p FROM Product p WHERE p.id = " + id, Product.class).getResultList().getFirst();
    }

    public void save(Product product) {
        entityManager.getTransaction().begin();

        try {
            entityManager.persist(product);
            entityManager.getTransaction().commit();
        }catch (Exception e) {
            entityManager.getTransaction().rollback();
            System.out.println(e.getMessage());
            System.out.println("Product may exist in the database.");
        }
    }
}
