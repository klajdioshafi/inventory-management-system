package validator;

import dto.ProductRequest;

public class ProductValidator {

    public static void validateProduct(ProductRequest productRequest) {

        if(productRequest.getTitle().isBlank()) {
            throw new IllegalArgumentException();
        }

        if(productRequest.getPrice() <= 0D) {
            throw new IllegalArgumentException();
        }

        if(productRequest.getQuantity() <= 0) {
            throw new IllegalArgumentException();
        }
    }
}
