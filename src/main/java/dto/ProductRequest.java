package dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {

    private String title;

    private String description;

    private Integer quantity;

    private Double price;

    private Long categoryId;
}
