package client;

import dto.CategoryRequest;
import dto.ProductRequest;
import entity.Product;
import service.ProductServiceImpl;

import java.util.List;
import java.util.Scanner;

public class InventoryManagementSystem {

    private static final ProductServiceImpl productServiceImpl = new ProductServiceImpl();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Inventory Management System");
        System.out.println("1. Add product");
        System.out.println("2. Display all products");
        System.out.println("3. Notify for low stock product");
        System.out.println("4. Add category");
        System.out.println("5. display all categories");

        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                createProduct();
                break;
            case 2:
                //displayAllProducts
                break;
            case 4:
                createCategory();
            default:

        }
    }

    public static void createProduct() {
        Scanner scanner = new Scanner(System.in);
        ProductRequest productRequest = new ProductRequest();
        System.out.println("Please enter product name: \n");
        String title = scanner.nextLine();
        productRequest.setTitle(title);
        System.out.println("Please enter product description: \n");
        String description = scanner.nextLine();
        productRequest.setDescription(description);
        System.out.println("Please enter product quantity: \n");
        int quantity = scanner.nextInt();
        productRequest.setQuantity(quantity);
        System.out.println("Please enter product price: \n");
        double price = scanner.nextDouble();
        productRequest.setPrice(price);
        System.out.println("Please enter product category: \n");
        long categoryId = scanner.nextLong();
        productRequest.setCategoryId(categoryId);

        productServiceImpl.createProduct(productRequest);
    }

    public static void createCategory() {
        Scanner scanner = new Scanner(System.in);
        CategoryRequest categoryRequest = new CategoryRequest();
        System.out.println("Please enter category name: \n");
        String title = scanner.nextLine();
        categoryRequest.setTitle(title);

        productServiceImpl.createCategory(categoryRequest);
    }

    public static List<Product> displayAllProducts() {
        return productServiceImpl.notifyForLowStock();
    }
}