package util;

import entity.Category;
import entity.Product;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

//    public static SessionFactory buildSessionFactory() {
//
//        try {
//            return new Configuration().configure("hibernate.cfg.xml")
//                    .buildSessionFactory();
//        }catch (ExceptionInInitializerError e) {
//            System.out.println(e.getMessage());
//            throw new ExceptionInInitializerError();
//        }
//    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Category.class);
                configuration.addAnnotatedClass(Product.class);
                configuration.configure("hibernate.cfg.xml");

                // Hibernate settings equivalent to hibernate.cfg.xml's properties

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return sessionFactory;
    }
}